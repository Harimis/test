gulp = require 'gulp'
browserSync = require('browser-sync').create()
historyApiFallback = require('connect-history-api-fallback')
argv = require('yargs').argv

$ = require('gulp-load-plugins')
    camelize: true
    lazy: true

$.wiredep = require('wiredep').stream

gulp.task 'clean', -> del ["dist/*", "!dist/assets", "!dist/bower_components"]

gulp.task 'serve', ['compile'], ->
    browserSync.init [
        './dist/*.html'
        './dist/js/**/*'
        './dist/css/**/*'
        './dist/views/**/*'
    ],
        server:
            baseDir: './dist'
            middleware: historyApiFallback index: '/index.html'
            index: 'index.html'

    $.watch 'src/**/*.styl', read: false, -> gulp.start ['compile.styl']
    $.watch 'src/**/*.coffee', read: false, -> gulp.start ['compile.coffee']
    $.watch ['src/**/*.jade', '!src/index.jade'], read: false, -> gulp.start ['compile.jade']
    $.watch 'src/index.jade', read: false, -> gulp.start ['compile.index']

gulp.task 'compile', ['compile.styl', 'compile.coffee', 'compile.jade', 'compile.index']

gulp.task 'compile.styl', ['filenames.styl'], ->
    gulp.src 'src/**/*.styl'
    .pipe $.plumber()
    .pipe $.stylus
        sourcemap: inline: true
    .pipe gulp.dest 'dist/css'

gulp.task 'compile.coffee', ['filenames.coffee'], ->
    gulp.src 'src/**/*.coffee'
    .pipe $.sourcemaps.init()
    .pipe $.plumber()
    .pipe $.coffee()
    .pipe $.sourcemaps.write()
    .pipe gulp.dest 'dist/js'

gulp.task 'compile.jade', ['filenames.jade'], ->
    gulp.src ['src/**/*.jade', '!src/index.jade']
    .pipe $.plumber()
    .pipe $.jade
        pretty: true
    .pipe gulp.dest 'dist/views'

bower = ->
    $.wiredep
        ignorePath: '../dist/'
        devDependencies: true
        fileTypes: html: replace:
            js: '<script src="{{filePath}}"></script>'
            css: '<link rel="stylesheet" href="{{filePath}}" />'

getInsert = ->
    insertJs: $.filenames.get('coffee').map (filename) ->
        jsPath = filename.replace('.coffee', '.js')
        jsPath = jsPath.replace(/\\/g, '/') if argv.win
        "<script src=\"js/#{jsPath}\"></script>"
    insertCss: $.filenames.get('styl').map (filename) ->
        cssPath = filename.replace('.styl', '.css')
        cssPath = cssPath.replace(/\\/g, '/') if argv.win
        "<link rel=\"stylesheet\" href=\"css/#{cssPath}\">"

gulp.task 'compile.index', ['filenames.coffee', 'filenames.styl'], ->
    gulp.src 'src/index.jade'
    .pipe $.jade
        pretty: true
    .pipe bower()
    .pipe $.htmlReplace getInsert(), keepBlockTags: true
    .pipe gulp.dest 'dist'

['coffee','styl','jade'].forEach (type) ->
    gulp.task "filenames.#{type}", ->
        $.filenames.forget type
        gulp.src ["src/**/*.#{type}", '!src/index.jade']
        .pipe $.filenames type

gulp.task 'default', ['serve']
