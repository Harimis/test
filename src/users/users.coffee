angular.module 'test'
.controller 'UsersCtrl', ($scope, users) ->
    $scope.users = users.data.users
    $scope.groups = users.data.groups
    $scope.activeView = 'table'

    $scope.fields = [
        id: 'name'
        title: 'Полное имя'
    ,
        id: 'profileLink'
        title: 'Учётная запись'
    ,
        id: 'email'
        title: 'Электронная почта'
    ,
        id: 'group'
        title: 'Группа'
    ,
        id: 'phone'
        title: 'Номер телефона'
    ]

    $scope.sort =
        field: $scope.fields[0]
        desc: false

    $scope.changeSort = (field) ->
        if field is $scope.sort.field
            $scope.sort.desc = not $scope.sort.desc
        else
            $scope.sort.field = field
            $scope.sort.desc = false

.filter 'userSearch', () ->
    (users, search) ->
        return users unless search
        filteredUsers = []
        _.forEach users, (user) ->
            search = _.toLower search
            if _.toLower(user.email).indexOf(search) > -1 or _.toLower(user.name).indexOf(search) > -1 or _.toLower(user.phone).indexOf(search) > -1
                filteredUsers.push user
        filteredUsers

.filter 'userGroupFilter', () ->
    (users, group) ->
        filteredUsers = []
        _.forEach users, (user) ->
            if group is user.group - 1
                filteredUsers.push user
        filteredUsers
