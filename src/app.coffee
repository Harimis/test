angular.module 'test', ['ngRoute', 'ui.bootstrap']
.config ($routeProvider, $locationProvider, $httpProvider) ->
    $locationProvider.html5Mode
        enabled: true
        requireBase: true

    $httpProvider.defaults.withCredentials = true
    $httpProvider.defaults.headers.common.Accept = "application/json"

    $routeProvider.caseInsensitiveMatch = true

    tmpl = (name) ->
        if /\//.test name
            "/views/#{name}.html"
        else
            "/views/#{name}/#{name}.html"

    $routeProvider.when '/', templateUrl: tmpl('homepage'), resolve:
        pageTitle: ($rootScope) ->
            $rootScope.pageTitle = 'Главная'

    $routeProvider.when '/users', templateUrl: tmpl('users'), controller: 'UsersCtrl', resolve:
        pageTitle: ($rootScope) ->
            $rootScope.pageTitle = 'Пользователи'
        users: ($http) ->
            $http.get 'http://www.json-generator.com/api/json/get/bPpPLABQQy?indent=2', withCredentials: false

    $routeProvider.otherwise redirectTo: '/'

.factory 'config', ->
    api: window.location.protocol + '//' + ('/* @echo API_LINK */' or 'api.stage.zavento.com')
