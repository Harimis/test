angular.module 'test'
.directive 'appHeader', ->
    restrict: 'E'
    replace: true
    templateUrl: '/views/app-header/app-header.html'
    link: (scope)->
        scope.navLinks = [
            title: 'Главная',
            link: '/'
        ,
            title: 'Пользователи',
            link: '/users'
        ]
